# reactive-banana

0[R](https://repology.org/project/haskell:reactive-banana/versions)DeNi Library for functional reactive programming (FRP)

* [HeinrichApfelmus/reactive-banana](https://github.com/HeinrichApfelmus/reactive-banana)

# Official documentation
* [Reactive-banana](https://wiki.haskell.org/Reactive-banana)
* [FRP explanation using reactive-banana](https://wiki.haskell.org/FRP_explanation_using_reactive-banana)
* [Examples](https://wiki.haskell.org/Reactive-banana/Examples)
* Haskell version of Sodium functional reactive programming library [+](https://github.com/SodiumFRP/sodium)
* [*apfeλmus*](https://apfelmus.nfshost.com/)

# Packages
* [reactive-banana-gi-gtk](http://hackage.haskell.org/package/reactive-banana-gi-gtk)
* [reactive-banana-wx](https://hackage.haskell.org/package/reactive-banana-wx)

# Similar libraries in other languages
* [SodiumFRP/sodium](https://github.com/SodiumFRP/sodium)
  * Purescript
  * Rust
  * Scala
  * ...
